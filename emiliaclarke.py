#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import discord
import asyncio
import yaml
import random
import aiohttp
from discord.ext import commands


### Configuration file
with open('config.yml', 'r') as ymlfile:
    cfg = yaml.load(ymlfile)


### Objects
bot = commands.Bot(command_prefix='!')

@bot.event
async def on_ready():
    print('------')
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command()
async def ping():
    ''' Replies with pong to ping. '''
    await bot.say('Pong!')

@bot.command()
async def pong():
    ''' Replies with ping to pong. '''
    await bot.say('Ping!')

@bot.command()
async def echo(*, message : str):
    ''' Simply repeats what you said'''
    await bot.say(message)

@bot.command()
async def dice_roll():
    ''' Return a random number equal or between 1 and 6 '''
    await bot.say(random.randint(1, 6))

@bot.command()
async def weather(country, county, municipality, city):
    ''' Checks the weather for a city '''
    url = 'http://www.yr.no/sted/' + country + '/' + county + '/' + \
                                municipality + '/' + city + '/varsel.xml'
    await bot.say(url)

    async with aiohttp(url) as response:
        pass

    await bot.say('Værvarsel fra Yr levert av Meteorologisk institutt og NRK.')

@bot.command()
async def menu():
    await bot.say('!pong, !ping, !echo, !weather')

bot.run(cfg['token'])
